import Application from 'rock-n-roll-ember/app';
import config from 'rock-n-roll-ember/config/environment';
import { setApplication } from '@ember/test-helpers';
import { start } from 'ember-qunit';

setApplication(Application.create(config.APP));

start();
